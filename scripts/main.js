$(document).ready(function() {
    // Sticky navigation
    var stickyNavTop = $('.big_nav').offset().top;
    var stickyNav = function(){
        var scrollTop = $(window).scrollTop();
            
        if (scrollTop > stickyNavTop) { 
            $('.big_nav').addClass('sticky');
        } else {
            $('.big_nav').removeClass('sticky'); 
        }
    };

    $(window).scroll(function() {
        stickyNav();
    });
})