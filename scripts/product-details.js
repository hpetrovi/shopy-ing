$(document).ready(function(){
    // Increment and decrement qunatity
    var quantityCount = 1;
    $('#product-details  .plus').click(function(){
        quantityCount++;

        $('#product-details .count').text(quantityCount);
    });

    $('#product-details .minus').click(function(){
        quantityCount == 0 || quantityCount--;

        $('#product-details .count').text(quantityCount);
    });

    // Gallery interaction
    $('#product-details .small_images .image').hover(function(){
        $('#product-details .big_image img').attr('src', $(this).find('img').attr('src'));
    });
});