$(document).ready(function(){
    // Prevents map from catching scroll movement of the mouse
    // When map is clicked scroll/zoom is given to the map, and upon exit it is take again
    $('#google_map')
	.click(function(){
			$(this).find('.map').addClass('clicked')})
	.mouseleave(function(){
			$(this).find('.map').removeClass('clicked')});
})