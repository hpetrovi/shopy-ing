$(document).ready(function () {
    // Initial render
    var storedInitCount = localStorage.getItem('initCountHome') || 4;

    printProducts(parseInt(storedInitCount))

    $('#init_product_count').val(storedInitCount);


    $('#init_product_count').change(function(value) {
        localStorage.setItem('initCountHome', $(this).val())
    });

    // Load more event bind
    $('#new_arrivals .load_more').click(printProducts.bind(null, 4));

    function printProducts(count) {
        var products_html = '';


        for(var i = 1; i < count + 1; i++){
            var randomImgNumber = Math.ceil(Math.random() * 3)

            var product_html = '<div class="col-md-3">' +
                '<!--  Front side  -->' +
                '<div class="product-box">' +
                    '<div class="product">' +
                        '<div class="image">' +
                            '<img src=images/product-' + randomImgNumber + '.png>' +
                        '</div>' +
                        '<div class="product_title">Reebok Track Jacket</div>' +
                        '<div class="price">$100</div>' +
                    '</div>' +
                    '<div class="product_details">' +
                        '<div class="image">' +
                            '<img src="images/product-' + randomImgNumber + '.png">' +
                        '</div>' +
                        '<div class="product_title">Reebok Track Jacket</div>' +
                        '<div class="sizes">' +
                            'sizes &nbsp; : &nbsp; s - m - l - xl' +
                        '</div>' +
                        '<div class="colors">' +
                            '<div class="color red"></div> ' +
                            '<div class="color purple"></div> ' +
                            '<div class="color blue"></div> ' +
                            '<div class="color green"></div> ' +
                        '</div>' +
                        '<div class="splitter"></div>' +
                        '<div class="options">' +
                            '<div class="option globe"></div>' +
                            '<div class="option basket"></div>' +
                            '<div class="option heart"></div>' +
                        '</div>' +
                    '</div>' +
                '</div>' +
            '</div>';

            products_html += product_html;
        }


        $('#new_arrivals .products-box').append(products_html);
    }
});